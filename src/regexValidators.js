const invoiceTitleValidator =
  /^(?=.{6,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/;
const nameValidator = /^[a-zA-Z ]*$/;
const emailValidator = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
const pinCodeValidator = /^[1-9]{1}[0-9]{5}$/;
const indianMobileNumber = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[4-9]\d{9}$/;
const addressValidators = /^[#./0-9a-zA-Z\s,-]+$/;

export {
  invoiceTitleValidator,
  nameValidator,
  emailValidator,
  pinCodeValidator,
  indianMobileNumber,
  addressValidators,
};
