export const selectOptionsData = [
  {
    title: "None",
    value: "none",
    number: 0,
  },

  {
    title: "Custom",
    value: "custom",
    number: 0,
  },
  {
    title: "On Receipt",
    value: "onReceipt",
    number: 0,
  },

  {
    title: "Next Day",
    value: "nextDay",
    number: 1,
  },
  {
    title: "2 Days",
    value: "days2",
    number: 2,
  },
  {
    title: "3 Days",
    value: "days3",
    number: 3,
  },
  {
    title: "4 Days",
    value: "days4",
    number: 4,
  },
  {
    title: "5 Days",
    value: "days5",
    number: 5,
  },
  {
    title: "6 Days",
    value: "days6",
    number: 6,
  },
  {
    title: "7 Days",
    value: "days7",
    number: 7,
  },
  {
    title: "10 Days",
    value: "days10",
    number: 10,
  },

  {
    title: "14 Days",
    value: "days14",
    number: 14,
  },
  {
    title: "21 Days",
    value: "days21",
  },
  {
    title: "30 Days",
    value: "days30",
    number: 30,
  },
  {
    title: "45 Days",
    value: "days45",
    number: 45,
  },
  {
    title: "60 Days",
    value: "days60",
    number: 60,
  },
  {
    title: "90 Days",
    value: "days90",
    number: 90,
  },
  {
    title: "120 Days",
    value: "days120",
    number: 120,
  },
  {
    title: "180 Days",
    value: "days180",
    number: 180,
  },
  {
    title: "365 Days",
    value: "days365",
    number: 365,
  },
];

export const columns = [
  {
    title: "Item",
    dataIndex: "itemDescription",
  },
  {
    title: "Rate",
    dataIndex: "rate",
  },
  {
    title: "QTY",
    dataIndex: "quantity",
  },
  {
    title: "Amount",
    dataIndex: "amount",
  },
];
