import {
  DISPLAY_ITEMS,
  ADD_ITEM,
  DELETE_ITEM,
  UPDATE_ITEM_DESCRIPTION,
  UPDATE_ADDTIONAL_DETAILS,
  ITEM_RATE,
  ITEM_QUANTITY,
  ITEM_TOTAL,
  UPDATE_TAX_CHECK,
} from "./namedActions";

export const displayAllItems = () => {
  return {
    type: DISPLAY_ITEMS,
  };
};
export const addAItem = () => {
  return {
    type: ADD_ITEM,
  };
};
export const deleteAItem = (itemId) => {
  return {
    type: DELETE_ITEM,
    payload: itemId,
  };
};

export const updateItemDescription = (itemDes,itemId) => {
  return {
    type: UPDATE_ITEM_DESCRIPTION,
    id: itemId,
    payload: itemDes,
  };
};
export const updateAdditionalDetails = (AdditionalItemDes,itemId) => {
  return {
    type: UPDATE_ADDTIONAL_DETAILS,
    id: itemId,
    payload: AdditionalItemDes,
  };
};
export const updateItemRate = (itemRate,itemId) => {
  return {
    type: ITEM_RATE,
    id: itemId,
    payload: itemRate,
  };
};
export const updateItemQuantity = (itemQty,itemId) => {
  return {
    type: ITEM_QUANTITY,
    id: itemId,
    payload: itemQty,
  };
};
export const updateItemTotal = (itemId) => {
  return {
    type: ITEM_TOTAL,
    id: itemId,
  };
};
export const UpdateItemTaxCheck = (itemTax,itemId) => {
  return {
    type: UPDATE_TAX_CHECK,
    id: itemId,
    payload: itemTax,
  };
};


