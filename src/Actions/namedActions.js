export const DISPLAY_ITEMS = 'items/displayAllItemsDetails';
export const ADD_ITEM = 'items/addItem';
export const DELETE_ITEM = 'items/deleteItem';

export const UPDATE_ITEM_DESCRIPTION = 'items/updateItemDescription';
export const UPDATE_ADDTIONAL_DETAILS = 'items/updateAdditionalDetails';
export const ITEM_RATE = 'items/updateItemRate';
export const ITEM_QUANTITY = 'items/updateItemQuantity';
export const ITEM_TOTAL = 'items/updateItemTotal';
export const UPDATE_TAX_CHECK = 'items/UpdateItemTaxCheck';
