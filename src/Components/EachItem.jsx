import React, { useEffect, useState } from "react";

// import {
//   deleteAItem,
//   updateItemDescription,
//   updateAdditionalDetails,
//   updateItemRate,
//   updateItemQuantity,
//   updateItemTotal,
//   UpdateItemTaxCheck,
// } from "../Actions/actionCreators";
// import { connect } from "react-redux";
// {
//     id: uuidv4(),
//     itemDescription: "",
//     addtionalDetails: "",
//     rate: 0.0,
//     quantity: 1,
//     amount: 0.0,
//     taxCheck: false,
//   };
const errorInitialData = {
  itemDescriptionCheck: false,
  itemDescriptionErrorText: "",
  rateCheck: false,
  rateErrorText: "",
  quantityCheck: true,
  quantityErrorText: "",
};

/*



*/
function EachItem(props) {
  const {
    itemDetails,
    deleteItem,
    itemDescription,
    itemAdditionalDetails,
    itemRate,
    itemQuantity,
    // itemTotal,
    itemTaxCheck,
    itemsCheckChange
  } = props;
  const [error, setError] = useState(errorInitialData);
useEffect(() => {
  itemsCheckChange(error.itemDescriptionCheck && error.rateCheck && error.quantityCheck,itemDetails.id)
},[error])
  const itemId = itemDetails.id;

  const handleItemDelete = () => {
    deleteItem(itemId);
    // dispatch(deleteAItem(itemId));
  };
  const handleItemDescription = (event) => {
    setError({
      ...error,
      itemDescriptionCheck: !event.target.value ? false : true,
      itemDescriptionErrorText: !event.target.value
        ? "* Item should not be empty"
        : "",
    });
    itemDescription(event.target.value, itemId);
    // dispatch(updateItemDescription(event.target.value, itemId));
  };
  const handleAddtionalDetails = (event) => {
    itemAdditionalDetails(event.target.value, itemId);
    // dispatch(updateAdditionalDetails(event.target.value, itemId));
  };

  const handleRate = (event) => {
    let rateCheckCondition =
      !event.target.value || !(Number(event.target.value) > 0);
    setError({
      ...error,
      rateCheck: rateCheckCondition ? false : true,
      rateErrorText: rateCheckCondition ? "* Enter value greater than 0" : "",
    });
    itemRate(event.target.value, itemId);
    // itemTotal(itemId);
  };

  const handleQuatity = (event) => {
    let quantityCheckCondition =
      !event.target.value || !(Number(event.target.value) > 0);
    setError({
      ...error,
      quantityCheck: quantityCheckCondition ? false : true,
      quantityErrorText: quantityCheckCondition
        ? "* Enter value greater than 0"
        : "",
    });
    itemQuantity(event.target.value, itemId);
    // itemTotal(itemId);
  };

  const handleTaxCheck = (event) => {
    itemTaxCheck(event.target.checked, itemId);
  };

  const handleItemDescriptionCheck = () => {
    setError({
      ...error,
      itemDescriptionCheck: !itemDetails.itemDescription ? false : true,
      itemDescriptionErrorText: !itemDetails.itemDescription
        ? "* Item should not be empty"
        : "",
    });
  };
  const handleRateCheck = () => {
    let rateCheckCondition =
      !itemDetails.rate || !(Number(itemDetails.rate) > 0);
    setError({
      ...error,
      rateCheck: rateCheckCondition ? false : true,
      rateErrorText: rateCheckCondition ? "* Enter value greater than 0" : "",
    });
  };
  const handleQuantityCheck = () => {
    let quantityCheckCondtion =
      !itemDetails.quantity || !(Number(itemDetails.quantity) > 0);
    setError({
      ...error,
      quantityCheck: quantityCheckCondtion ? false : true,
      quantityErrorText: quantityCheckCondtion
        ? "* Enter value greater than 0"
        : "",
    });
  };
  return (
    <ul className="d-flex p-0 invoice-items-main-container pb-3 justify-content-around">
      <li className="invoice-line-style-none">
        <button
          className="invoice-item-close-button d-flex"
          onClick={handleItemDelete}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="currentColor"
            className="bi bi-x-lg"
            viewBox="0 0 16 16"
          >
            <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z" />
          </svg>
        </button>
      </li>
      <li className="invoice-line-style-none invoice-width-40 d-flex flex-column">
        <input
          className="form-control "
          onChange={handleItemDescription}
          type="text"
          placeholder="Item Description"
          value={itemDetails.itemDescription}
          onBlur={handleItemDescriptionCheck}
        />
        <p className="m-0 text-danger error-text">
          {error.itemDescriptionErrorText}
        </p>
        <textarea
          className="form-control mt-2"
          onChange={handleAddtionalDetails}
          type="text"
          placeholder="Additional details"
          value={itemDetails.addtionalDetails}
        />
      </li>
      <li className="invoice-line-style-none invoice-width-10">
        <input
          onChange={handleRate}
          className="text-end form-control"
          type="text"
          value={itemDetails.rate}
          placeholder="0.00"
          onBlur={handleRateCheck}
        />
        <p className="m-0 text-danger error-text">{error.rateErrorText}</p>
      </li>
      <li className="invoice-line-style-none invoice-width-10">
        <input
          onChange={handleQuatity}
          className="text-end form-control"
          type="text"
          value={itemDetails.quantity}
          onBlur={handleQuantityCheck}
        />
        <p className="m-0 text-danger error-text">{error.quantityErrorText}</p>
      </li>
      <li className="invoice-line-style-none">
        <span>
          ₹
          {error.rateCheck && error.quantityCheck
            ? itemDetails.amount
            : (0.0).toFixed(2)}
        </span>
      </li>
      <li className="invoice-line-style-none">
        <input
          onChange={handleTaxCheck}
          type="checkbox"
          checked={itemDetails.taxCheck}
        />
      </li>
    </ul>
  );
}

export default EachItem;
