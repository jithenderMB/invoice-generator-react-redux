import React, { useEffect, useState } from "react";

import InvoiceTitleSection from "./InvoiceTitleSection";
import FromAndBillToSection from "./FromAndBillToSection";
import DatesSection from "./DatesSection";
import InvoiceItemsSection from "./InvoiceItemsSection";

import { v4 as uuidv4 } from "uuid";

import {
  invoiceTitleValidator,
  nameValidator,
  emailValidator,
  pinCodeValidator,
  indianMobileNumber,
  addressValidators,
} from "../regexValidators";
import { selectOptionsData } from "../data.js";
import { displayAllItems, addAItem } from "../Actions/actionCreators";
// import { connect } from "react-redux";
import { produce } from "immer";

import "antd/dist/antd.css";
import moment from "moment";

const itemsInitialData = () => {
  return {
    id: uuidv4(),
    itemDescription: "",
    addtionalDetails: "",
    rate: "",
    quantity: 1,
    amount: 0,
    taxCheck: false,
  };
};

const initialData = {
  InvoiceTitle: "Invoice",
  from: {
    name: "",
    email: "",
    address: {
      street: "",
      cityState: "",
      pinCode: "",
    },
    phone: "",
    gstNumber: "",
  },
  billTo: {
    name: "",
    email: "",
    address: {
      street: "",
      cityState: "",
      pinCode: "",
    },
    phone: "",
  },
  dataTime: {
    number: "",
    date: "",
    terms: "",
    dueDate: "",
  },
  invoiceNotes: "",
  items: [itemsInitialData()],
};
const errorInitialData = {
  InvoiceTitleCheck: true,
  InvoiceTitleErrorText: "",
  fromValid: {
    nameCheck: false,
    nameErrorText: "",
    emailCheck: false,
    emailErrorText: "",
    phoneCheck: false,
    phoneErrorText: "",
    streetCheck: false,
    streetErrorText: "",
    cityStateCheck: false,
    cityStateErrorText: "",
    pinCodeCheck: false,
    pincodeErrorText: "",
  },
  billToValid: {
    nameCheck: false,
    nameErrorText: "",
    emailCheck: false,
    emailErrorText: "",
    phoneCheck: false,
    phoneErrorText: "",
    streetCheck: false,
    streetErrorText: "",
    cityStateCheck: false,
    cityStateErrorText: "",
    pinCodeCheck: false,
    pincodeErrorText: "",
  },
  itemsCheck: {},
};

function InvoicePage(props) {
  const [data, setData] = useState(initialData);
  const [error, setError] = useState(errorInitialData);
  const [startDate, setStartDate] = useState(moment());
  const [dueDate, setDueDate] = useState(moment());
  const [selectOption, setSelectOption] = useState("onReceipt");
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [finalInvoiceButton,setFinalInvoiceButton] = useState(false);
  const [finalInvoiceError,setFinalInvoiceError] = useState('');


  const handleItemsCheck = (value, id) => {
    setError(
      produce(error, (newError) => {
        newError.itemsCheck = { ...error.itemsCheck, [id]: value };
      })
    );
  };
  const handleOk = () => {
    setIsModalVisible(false);
    setData(initialData);
    setError(errorInitialData);
    setData(initialData);
    setError(errorInitialData);
    setStartDate(moment());
    setDueDate(moment());
    setSelectOption("onReceipt");
    props.dispatch(displayAllItems());
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const addNewItem = () => {
    setData(
      produce(data, (newData) => {
        newData.items = [...data.items, itemsInitialData()];
      })
    );
  };

  const handleSubmit = (event) => {
    event.preventDefault();
  };

  const handleFromPhoneNumber = (event) => {
    setError(
      produce(error, (newError) => {
        newError.fromValid.phoneCheck = indianMobileNumber.test(
          event.target.value
        );
      })
    );

    setData(
      produce(data, (newData) => {
        newData.from.phone = event.target.value;
      })
    );
  };
  const handleBillToPhoneNumber = (event) => {
    setError(
      produce(error, (newError) => {
        newError.billToValid.phoneCheck = indianMobileNumber.test(
          event.target.value
        );
      })
    );
    setData(
      produce(data, (newData) => {
        newData.billTo.phone = event.target.value;
      })
    );
  };

  const handleBillToStreet = (event) => {
    setError(
      produce(error, (newError) => {
        newError.billToValid.streetCheck = addressValidators.test(
          event.target.value
        );
        if (!event.target.value) {
          newError.billToValid.cityStateErrorText = "";
          newError.billToValid.pincodeErrorText = "";
        } else {
          newError.billToValid.pincodeErrorText = data.billTo.address.pinCode
            ? "* Enter a valid pin code"
            : "";
          newError.billToValid.cityStateErrorText = data.billTo.address
            .cityState
            ? "* Enter a valid pin code"
            : "";
        }
      })
    );
    setData(
      produce(data, (newData) => {
        newData.billTo.address.street = event.target.value;
      })
    );
  };
  const handleFromStreet = (event) => {
    setError(
      produce(error, (newError) => {
        newError.fromValid.streetCheck = addressValidators.test(
          event.target.value
        );
        if (!event.target.value) {
          newError.fromValid.cityStateErrorText = "";
          newError.fromValid.pincodeErrorText = "";
        } else {
          newError.fromValid.pincodeErrorText = data.from.address.pinCode
            ? "* Enter a valid pin code"
            : "";
          newError.fromValid.cityStateErrorText = data.from.address.cityState
            ? "* Enter a valid City/State"
            : "";
        }
      })
    );
    setData(
      produce(data, (newData) => {
        newData.from.address.street = event.target.value;
      })
    );
  };

  const handleFromName = (event) => {
    setError(
      produce(error, (newError) => {
        newError.fromValid.nameCheck = nameValidator.test(event.target.value);
      })
    );
    setData(
      produce(data, (newData) => {
        newData.from.name = event.target.value;
      })
    );
  };
  const handleFromEmail = (event) => {
    setError(
      produce(error, (newError) => {
        newError.fromValid.emailCheck = emailValidator.test(event.target.value);
      })
    );

    setData(
      produce(data, (newData) => {
        newData.from.email = event.target.value;
      })
    );
  };
  const handleBillToName = (event) => {
    setError(
      produce(error, (newError) => {
        newError.billToValid.nameCheck = nameValidator.test(event.target.value);
      })
    );
    setData(
      produce(data, (newData) => {
        newData.billTo.name = event.target.value;
      })
    );
  };
  const handleBillToEmail = (event) => {
    setError(
      produce(error, (newError) => {
        newError.billToValid.emailCheck = emailValidator.test(
          event.target.value
        );
      })
    );

    setData(
      produce(data, (newData) => {
        newData.billTo.email = event.target.value;
      })
    );
  };
  const handleFromCityState = (event) => {
    setError(
      produce(error, (newError) => {
        newError.fromValid.cityStateCheck = addressValidators.test(
          event.target.value
        );
      })
    );

    setData(
      produce(data, (newData) => {
        newData.from.address.cityState = event.target.value;
      })
    );
  };
  const handleFromPincode = (event) => {
    setError(
      produce(error, (newError) => {
        newError.fromValid.pinCodeCheck = pinCodeValidator.test(
          event.target.value
        );
        if (!event.target.value) {
          newError.fromValid.pincodeErrorText = "";
        }
      })
    );
    setData(
      produce(data, (newData) => {
        newData.from.address.pinCode = event.target.value;
      })
    );
  };

  const handleBillToCityState = (event) => {
    setError(
      produce(error, (newError) => {
        newError.billToValid.cityStateCheck = addressValidators.test(
          event.target.value
        );
      })
    );

    setData(
      produce(data, (newData) => {
        newData.billTo.address.cityState = event.target.value;
      })
    );
  };
  const handleBillToPincode = (event) => {
    setError(
      produce(error, (newError) => {
        newError.billToValid.pinCodeCheck = pinCodeValidator.test(
          event.target.value
        );
      })
    );
    setData(
      produce(data, (newData) => {
        newData.billTo.address.pinCode = event.target.value;
      })
    );
  };
  const handleInvoiceTitle = (event) => {
    setError(
      produce(error, (newError) => {
        newError.InvoiceTitleCheck = invoiceTitleValidator.test(
          event.target.value
        );
        newError.InvoiceTitleErrorText = invoiceTitleValidator.test(
          event.target.value
        )
          ? ""
          : "* Enter a valid invoice title, 6-20 characters, a-z,A-Z,0-9";
      })
    );
    setData(
      produce(data, (newData) => {
        newData.InvoiceTitle = event.target.value;
      })
    );
  };
  const handleFormEmailCheck = () => {
    const errorMessage = error.fromValid.emailCheck
      ? ""
      : "* Enter a valid email";
    setError(
      produce(error, (newError) => {
        newError.fromValid.emailErrorText = errorMessage;
      })
    );
  };
  const handleFormPincodeCheck = () => {
    const errorMessage = error.fromValid.pinCodeCheck
      ? ""
      : "* Enter a valid pin code";
    setError(
      produce(error, (newError) => {
        newError.fromValid.pincodeErrorText = errorMessage;
      })
    );
  };
  const handleFormPhoneCheck = () => {
    const errorMessage = error.fromValid.phoneCheck
      ? ""
      : "* Enter a valid phone";
    setError(
      produce(error, (newError) => {
        newError.fromValid.phoneErrorText = errorMessage;
      })
    );
  };
  const handleBillToEmailCheck = () => {
    const errorMessage = error.billToValid.emailCheck
      ? ""
      : "* Enter a valid email";
    setError(
      produce(error, (newError) => {
        newError.billToValid.emailErrorText = errorMessage;
      })
    );
  };
  const handleBillToPincodeCheck = () => {
    const errorMessage = error.billToValid.pinCodeCheck
      ? ""
      : "* Enter a valid pin code";
    setError(
      produce(error, (newError) => {
        newError.billToValid.pincodeErrorText = errorMessage;
      })
    );
  };
  const handleBillToPhoneCheck = () => {
    const errorMessage = error.billToValid.phoneCheck
      ? ""
      : "* Enter a valid phone";
    setError(
      produce(error, (newError) => {
        newError.billToValid.phoneErrorText = errorMessage;
      })
    );
  };
  const handleFormNameCheck = () => {
    const errorMessage = error.fromValid.nameCheck
      ? ""
      : "* Enter a valid name";
    setError(
      produce(error, (newError) => {
        newError.fromValid.nameErrorText = errorMessage;
      })
    );
  };
  const handleBillToNameCheck = () => {
    const errorMessage = error.billToValid.nameCheck
      ? ""
      : "* Enter a valid name";
    setError(
      produce(error, (newError) => {
        newError.billToValid.nameErrorText = errorMessage;
      })
    );
  };
  const handleInvoiceTitleCheck = () => {
    const errorMessage = error.InvoiceTitleCheck
      ? ""
      : "* Enter a valid invoice title, 6-20 characters, a-z,A-Z,0-9";
    setError(
      produce(error, (newError) => {
        newError.InvoiceTitleErrorText = errorMessage;
      })
    );
  };

  const handleSelect = (option) => {
    setSelectOption(option);
    if (option === "none" || option === "onReceipt") {
      setDueDate(moment());
    } else {
      selectOptionsData.forEach((eachOption) =>
        eachOption.value === option
          ? setDueDate(moment(startDate).add(eachOption.number, "days"))
          : null
      );
    }
  };
  const handleSubmitButton = () => {
    const { InvoiceTitleCheck, fromValid, billToValid } = error;

    if (
      !Object.values(error.itemsCheck).filter((value) => value === false)
        .length &&
      data.items.length &&
      InvoiceTitleCheck &&
      fromValid.nameCheck &&
      fromValid.emailCheck &&
      fromValid.phoneCheck &&
      fromValid.streetCheck &&
      fromValid.cityStateCheck &&
      fromValid.pinCodeCheck &&
      billToValid.nameCheck &&
      billToValid.emailCheck &&
      billToValid.phoneCheck &&
      billToValid.streetCheck &&
      billToValid.cityStateCheck &&
      billToValid.pinCodeCheck
    ) {
      let finalInvoiceData = produce(data, (newData) => {
        newData.dataTime.date = startDate.format("DD/MM/YYYY");
        newData.dataTime.terms = selectOption;
        newData.dataTime.dueDate =
          startDate.format("DD/MM/YYYY") === dueDate.format("DD/MM/YYYY")
            ? ""
            : dueDate.format("DD/MM/YYYY");
      });
      console.log(finalInvoiceData);
      setFinalInvoiceButton(true)
      setFinalInvoiceError('')
      setIsModalVisible(true);
      window.scrollTo(0, 0);
    } else {
      setFinalInvoiceButton(false)
      setFinalInvoiceError('* Enter valid data in all fields')
      window.scrollTo(0, 0);
      // alert("Fill all the details with valid data");
    }
  };

  const disabledDate = (current) => {
    // Can not select days before today and today
    return current && current < moment().endOf("day");
  };

  useEffect(() => {
    handleSelect(selectOption);
  }, [startDate]);
  const dateOnChange = (date) => {
    setStartDate(date);
  };

  const dueDateOnChange = (date) => {
    let days =
      moment(date).toString() === moment(startDate).toString()
        ? 0
        : moment
            .duration(moment(date).diff(moment(startDate)), "milliseconds")
            .asDays();
    setDueDate(date);
    parseInt(days) > 0
      ? selectOptionsData.forEach((option) =>
          option.number === parseInt(days)
            ? setSelectOption(option.value)
            : null
        )
      : setSelectOption("custom");
  };
  const handleInvoiceNotes = (event) => {
    setData(
      produce(data, (newData) => {
        newData.invoiceNotes = event.target.value;
      })
    );
  };

  const [fileList, setFileList] = useState([]);

  const onChange = ({ fileList: newFileList }) => {
    setFileList(newFileList);
  };

  const onPreview = async (file) => {
    let src = file.url;

    if (!src) {
      src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);

        reader.onload = () => resolve(reader.result);
      });
    }

    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow?.document.write(image.outerHTML);
  };

  const handleFromGstNumber = (event) => {
    setData(
      produce(data, (newData) => {
        newData.from.gstNumber = event.target.value;
      })
    );
  };
  const handleInvoiceNumber = (event) => {
    setData(
      produce(data, (newData) => {
        newData.dataTime.number = event.target.value;
      })
    );
  };

  const handleFromStreetValidations = () => {
    const errorMessage = error.fromValid.streetCheck
      ? ""
      : "* Enter a valid street address";
    setError(
      produce(error, (newError) => {
        newError.fromValid.streetErrorText = errorMessage;
      })
    );
  };

  const deleteAItem = (id) => {
    setData(
      produce(data, (newData) => {
        newData.items = data.items.filter((item) => item.id !== id);
      })
    );
  };

  const updateItemDescription = (description, id) => {
    setData(
      produce(data, (newData) => {
        newData.items = data.items.map((item) =>
          item.id === id ? { ...item, itemDescription: description } : item
        );
      })
    );
  };

  const updateAdditionalDetails = (details, id) => {
    setData(
      produce(data, (newData) => {
        newData.items = data.items.map((item) =>
          item.id === id ? { ...item, addtionalDetails: details } : item
        );
      })
    );
  };

  const updateItemRate = (itemRate, id) => {
    setData(
      produce(data, (newData) => {
        newData.items = data.items.map((item) =>
          item.id === id
            ? {
                ...item,
                rate: itemRate,
                amount: (itemRate * item.quantity * 1.0).toFixed(2),
              }
            : item
        );
      })
    );
  };

  const updateItemQuantity = (quantity, id) => {
    setData(
      produce(data, (newData) => {
        newData.items = data.items.map((item) =>
          item.id === id
            ? {
                ...item,
                quantity: quantity,
                amount: (item.rate * quantity * 1.0).toFixed(2),
              }
            : item
        );
      })
    );
  };

  const UpdateItemTaxCheck = (taxCheck, id) => {
    setData(
      produce(data, (newData) => {
        newData.items = data.items.map((item) =>
          item.id === id ? { ...item, taxCheck: taxCheck } : item
        );
      })
    );
  };
  const handleFromCityStateCheck = () => {
    const errorMessage = error.fromValid.cityStateCheck
      ? ""
      : "* Enter a valid City/State";
    setError(
      produce(error, (newError) => {
        newError.fromValid.cityStateErrorText = errorMessage;
      })
    );
  };
  const handleBillToCityStateCheck = () => {
    const errorMessage = error.billToValid.cityStateCheck
      ? ""
      : "* Enter a valid City/State";

    setError(
      produce(error, (newError) => {
        newError.billToValid.cityStateErrorText = errorMessage;
      })
    );
  };
  const handleBillToStreetValidations = () => {
    const errorMessage = error.billToValid.streetCheck
      ? ""
      : "* Enter a valid street address";
    setError(
      produce(error, (newError) => {
        newError.billToValid.streetErrorText = errorMessage;
      })
    );
  };
  return (
    <form className="invoice-main-container" onSubmit={handleSubmit}>
      <p className='text-danger'>{finalInvoiceError}</p>
      <InvoiceTitleSection
        invoicetitleChange={handleInvoiceTitle}
        invoicetitleBlur={handleInvoiceTitleCheck}
        InvoiceTitleCheck={error.InvoiceTitleCheck}
        InvoiceTitleErrorText={error.InvoiceTitleErrorText}
        fileListData={fileList}
        imageChange={onChange}
        imagePreview={onPreview}
        InvoiceTitle={data.InvoiceTitle}
      />
      <FromAndBillToSection
        // from details
        fromData={data.from}
        fromValid={error.fromValid}
        fromFunctions={{
          fromNameChange: handleFromName,
          fromNameBlur: handleFormNameCheck,
          fromEmailChange: handleFromEmail,
          fromEmailBlur: handleFormEmailCheck,
          fromStreetChange: handleFromStreet,
          fromStreetBlur: handleFromStreetValidations,
          fromCityStateChange: handleFromCityState,
          fromCityStateBlur: handleFromCityStateCheck,
          fromPinCodeChange: handleFromPincode,
          fromPinCodeBlur: handleFormPincodeCheck,
          fromPhoneChange: handleFromPhoneNumber,
          fromPhoneBlur: handleFormPhoneCheck,
          fromGstNumberChange: handleFromGstNumber,
        }}
        // bill to details
        billToData={data.billTo}
        billToValid={error.billToValid}
        billToFunctions={{
          billToNameChange: handleBillToName,
          billToNameBlur: handleBillToNameCheck,
          billToEmailChange: handleBillToEmail,
          billToEmailBlur: handleBillToEmailCheck,
          billToStreetChange: handleBillToStreet,
          billToStreetBlur: handleBillToStreetValidations,
          billToCityStateChange: handleBillToCityState,
          billToCityStateBlur: handleBillToCityStateCheck,
          billToPinCodeChange: handleBillToPincode,
          billToPinCodeBlur: handleBillToPincodeCheck,
          billToPhoneChange: handleBillToPhoneNumber,
          billToPhoneBlur: handleBillToPhoneCheck,
        }}
      />
      <hr />
      <DatesSection
        invoiceNumber={data.dataTime.number}
        invoiceNumberChange={handleInvoiceNumber}
        date={startDate}
        dateToBeDisabled={disabledDate}
        selectOnChnage={handleSelect}
        selectValue={selectOption}
        dueDateChange={dueDateOnChange}
        dueDate={dueDate}
        dateChange={dateOnChange}
      />
      <InvoiceItemsSection
        invoiceData={data}
        invoiceItems={data.items}
        addItem={addNewItem}
        deleteItem={deleteAItem}
        itemDescription={updateItemDescription}
        itemAdditionalDetails={updateAdditionalDetails}
        itemRate={updateItemRate}
        itemQuantity={updateItemQuantity}
        itemTaxCheck={UpdateItemTaxCheck}
        invoiceNotesChange={handleInvoiceNotes}
        generateInvoiceButton={handleSubmitButton}
        displayFinalInvoice={isModalVisible}
        confirmFinalInvoice={handleOk}
        closeFinalInvoice={handleCancel}
        date={startDate}
        dueDate={dueDate}
        itemsCheckChange={handleItemsCheck}
        finalInvoiceButton={finalInvoiceButton}
      />
    </form>
  );
}

// const mapStateToProps = (state) => {
//   return {
//     items: state.itemsReducer,
//   };
// };

// export default connect(mapStateToProps)(InvoicePage);
export default InvoicePage;
