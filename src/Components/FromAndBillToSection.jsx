import React from "react";

function FromAndBillToSection(props) {
  const {
    fromData,
    billToData,
    fromValid,
    billToValid,
    fromFunctions: {
      fromNameChange,
      fromNameBlur,
      fromEmailChange,
      fromEmailBlur,
      fromStreetChange,
      fromStreetBlur,
      fromCityStateChange,
      fromCityStateBlur,
      fromPinCodeChange,
      fromPinCodeBlur,
      fromPhoneChange,
      fromPhoneBlur,
      fromGstNumberChange,
    },
    billToFunctions: {
      billToNameChange,
      billToNameBlur,
      billToEmailChange,
      billToEmailBlur,
      billToStreetChange,
      billToStreetBlur,
      billToCityStateChange,
      billToCityStateBlur,
      billToPinCodeChange,
      billToPinCodeBlur,
      billToPhoneChange,
      billToPhoneBlur,
    },
  } = props;

  return (
    <div className="d-flex justify-content-between mt-4">
      <div className="invoice-from-billTo-main-container">
        <h4>From</h4>
        <ul className="p-0">
          <li className="invoice-sub-containers">
            <label htmlFor="fromName">Name</label>
            <div className="invoice-width-80">
              <input
                id="fromName"
                name="name"
                className="form-control"
                type="text"
                placeholder="Client Name"
                onChange={fromNameChange}
                value={fromData.name}
                onBlur={fromNameBlur}
              />
              <p
                className={
                  fromValid.nameCheck
                    ? "d-none"
                    : "m-0 text-danger invoice-error-text"
                }
              >
                {fromValid.nameErrorText}
              </p>
            </div>
          </li>
          <li className="invoice-sub-containers d-flex justify-content-between">
            <label htmlFor="fromEmail">Email</label>
            <div className="invoice-width-80">
              <input
                id="fromEmail"
                name="email"
                className="form-control "
                type="text"
                placeholder="name@client.com"
                onChange={fromEmailChange}
                value={fromData.email}
                onBlur={fromEmailBlur}
              />
              <p
                className={
                  fromValid.emailCheck
                    ? "d-none"
                    : "m-0 text-danger invoice-error-text"
                }
              >
                {fromValid.emailErrorText}
              </p>
            </div>
          </li>
          <li className="invoice-sub-containers d-flex flex-column">
            <div className="d-flex justify-content-between">
              <label htmlFor="fromStreet">Address</label>
              <div className="invoice-width-80">
                <input
                  id="fromStreet"
                  className="form-control"
                  type="text"
                  placeholder="Street"
                  onChange={fromStreetChange}
                  value={fromData.address.street}
                  onBlur={fromStreetBlur}
                />
                <p
                  className={
                    fromValid.streetCheck
                      ? "d-none"
                      : "m-0 text-danger invoice-error-text"
                  }
                >
                  {fromValid.streetErrorText}
                </p>
              </div>
            </div>

            <input
              type="text"
              placeholder="City,State"
              className={
                fromData.address.street
                  ? "align-self-end invoice-width-80 form-control mt-2"
                  : "d-none"
              }
              onChange={fromCityStateChange}
              value={fromData.address.cityState}
              onBlur={fromCityStateBlur}
            />
            <div className="invoice-width-80 align-self-end">
              <p
                className={
                  fromValid.cityStateCheck
                    ? "d-none"
                    : "m-0 text-danger invoice-error-text"
                }
              >
                {fromValid.cityStateErrorText}
              </p>
            </div>
            <input
              type="text"
              placeholder="Pin code"
              className={
                fromData.address.street
                  ? "align-self-end invoice-width-80 form-control mt-2"
                  : "d-none"
              }
              onChange={fromPinCodeChange}
              value={fromData.address.pinCode}
              onBlur={fromPinCodeBlur}
            />
            <div className="invoice-width-80 align-self-end">
              <p
                className={
                  fromValid.pinCodeCheck
                    ? "d-none"
                    : "m-0 text-danger invoice-error-text"
                }
              >
                {fromValid.pincodeErrorText}
              </p>
            </div>
          </li>
          <li className="invoice-sub-containers">
            <label htmlFor="">Phone</label>
            <div className="invoice-width-80">
              <input
                name="phone"
                className="form-control"
                type="text"
                placeholder="+91xxxxxxxxxx"
                onChange={fromPhoneChange}
                value={fromData.phone}
                onBlur={fromPhoneBlur}
              />

              <p
                className={
                  fromValid.phoneCheck
                    ? "d-none"
                    : "m-0 text-danger invoice-error-text"
                }
              >
                {fromValid.phoneErrorText}
              </p>
            </div>
          </li>
          <li className="invoice-sub-containers">
            <label htmlFor="">GST #</label>
            <input
              type="text"
              className="invoice-width-80 form-control"
              placeholder="123456789 RT"
              value={fromData.gstNumber}
              onChange={fromGstNumberChange}
            />
          </li>
        </ul>
      </div>
      <div className="invoice-from-billTo-main-container">
        <h4>Bill To</h4>
        <ul className="p-0">
          <li className="invoice-sub-containers">
            <label htmlFor="">Name</label>
            <div className="invoice-width-80">
              <input
                name="name"
                className="form-control"
                type="text"
                placeholder="Client Name"
                onChange={billToNameChange}
                value={billToData.name}
                onBlur={billToNameBlur}
              />
              <p
                className={
                  billToValid.nameCheck
                    ? "d-none"
                    : "m-0 text-danger invoice-error-text"
                }
              >
                {billToValid.nameErrorText}
              </p>
            </div>
          </li>

          <li className="invoice-sub-containers">
            <label htmlFor="">Email</label>
            <div className="invoice-width-80">
              <input
                name="email"
                className="form-control"
                type="text"
                placeholder="name@client.com"
                onChange={billToEmailChange}
                value={billToData.email}
                onBlur={billToEmailBlur}
              />
              <p
                className={
                  billToValid.emailCheck
                    ? "d-none"
                    : "m-0 text-danger invoice-error-text"
                }
              >
                {billToValid.emailErrorText}
              </p>
            </div>
          </li>
          <li className="invoice-sub-containers d-flex flex-column">
            <div className="d-flex justify-content-between">
              <label htmlFor="">Address</label>
              <div className="invoice-width-80">
                <input
                  className="form-control "
                  type="text"
                  placeholder="Street"
                  onChange={billToStreetChange}
                  value={billToData.address.street}
                  onBlur={billToStreetBlur}
                />
                <p
                  className={
                    billToValid.streetCheck
                      ? "d-none"
                      : "m-0 text-danger invoice-error-text"
                  }
                >
                  {billToValid.streetErrorText}
                </p>
              </div>
            </div>
            <input
              type="text"
              placeholder="City,State"
              className={
                billToData.address.street
                  ? "align-self-end invoice-width-80 form-control mt-2"
                  : "d-none"
              }
              onChange={billToCityStateChange}
              value={billToData.address.cityState}
              onBlur={billToCityStateBlur}
            />
            <div className="invoice-width-80 align-self-end">
              <p
                className={
                  billToValid.cityStateCheck
                    ? "d-none"
                    : "m-0 text-danger invoice-error-text"
                }
              >
                {billToValid.cityStateErrorText}
              </p>
            </div>
            <input
              type="text"
              placeholder="Pin code"
              className={
                billToData.address.street
                  ? "align-self-end invoice-width-80 form-control mt-2"
                  : "d-none"
              }
              onChange={billToPinCodeChange}
              value={billToData.address.pinCode}
              onBlur={billToPinCodeBlur}
            />
            <div className="invoice-width-80 align-self-end">
              <p
                className={
                  billToValid.pinCodeCheck
                    ? "d-none"
                    : "m-0 text-danger invoice-error-text"
                }
              >
                {billToValid.pincodeErrorText}
              </p>
            </div>
          </li>
          <li className="invoice-sub-containers">
            <label htmlFor="">Phone</label>
            <div className="invoice-width-80">
              <input
                name="phone"
                className="form-control"
                type="text"
                placeholder="+91xxxxxxxxxx"
                onChange={billToPhoneChange}
                value={billToData.phone}
                onBlur={billToPhoneBlur}
              />
              <p
                className={
                  billToValid.phoneCheck
                    ? "d-none"
                    : "m-0 text-danger invoice-error-text"
                }
              >
                {billToValid.phoneErrorText}
              </p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default FromAndBillToSection;
