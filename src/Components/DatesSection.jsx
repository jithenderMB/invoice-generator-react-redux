import React from "react";
import { selectOptionsData } from "../data.js";

import { DatePicker, Select } from "antd";
const { Option } = Select;

function DatesSection(props) {
  const {
    dateChange,
    invoiceNumber,
    invoiceNumberChange,
    date,
    dateToBeDisabled,
    selectOnChnage,
    selectValue,
    dueDateChange,
    dueDate,
  } = props;

  return (
    <div>
      <ul className="p-0 col-5 invoice-dates-main-container">
        <li className="invoice-sub-containers">
          <label htmlFor="">Number</label>
          <input
            type="text"
            className="form-control invoice-width-80"
            placeholder="INV0001"
            value={invoiceNumber}
            onChange={invoiceNumberChange}
          />
        </li>
        <li className="invoice-sub-containers d-flex justify-content-between">
          <label htmlFor="">Date</label>
          <div className="invoice-width-80">
            <DatePicker
              className="invoice-date-picker"
              format="DD/MM/YYYY"
              defaultValue={date}
              disabledDate={dateToBeDisabled}
              onChange={dateChange}
              value={date}
            />
          </div>
        </li>
        <li className="invoice-sub-containers">
          <label htmlFor="">Terms</label>
          <Select
            className="invoice-width-80"
            onChange={selectOnChnage}
            value={selectValue}
          >
            {selectOptionsData.map((option) => {
              return (
                <Option key={option.value} value={option.value}>
                  {option.title}
                </Option>
              );
            })}
          </Select>
        </li>
        <li
          className={
            
            selectValue === "none" || selectValue === "onReceipt"
              ? "d-none"
              : "invoice-sub-containers d-flex justify-content-between"
          }
        >
          <label htmlFor="">Due</label>
          <div className="invoice-width-80">
            <DatePicker
              className="invoice-date-picker"
              onChange={dueDateChange}
              format="DD/MM/YYYY"
              defaultValue={dueDate}
              value={dueDate}
              disabledDate={dateToBeDisabled}
            />
          </div>
        </li>
      </ul>
    </div>
  );
}

export default DatesSection;
