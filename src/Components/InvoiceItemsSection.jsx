import React from "react";
import { Button, Modal, Table } from "antd";
import EachItem from "./EachItem";
import { columns } from "../data";

function InvoiceItemsSection(props) {
  const {
    invoiceData: { invoiceNotes, InvoiceTitle, from, billTo, dataTime },
    invoiceItems,
    addItem,
    invoiceNotesChange,
    generateInvoiceButton,
    displayFinalInvoice,
    confirmFinalInvoice,
    closeFinalInvoice,
    date,
    dueDate,
    deleteItem,
    itemDescription,
    itemAdditionalDetails,
    itemRate,
    itemQuantity,
    // itemTotal,
    itemTaxCheck,
    itemsCheckChange,
    finalInvoiceButton
  } = props;

  return (
    <div className="mt-4">
      <ul className="d-flex invoice-items-table-options">
        <li className="invoice-line-style-none invoice-padding-13">
          DESCRIPTION
        </li>
        <li className="invoice-line-style-none invoice-padding-36">RATE</li>
        <li className="invoice-line-style-none invoice-padding-11-half">QTY</li>
        <li className="invoice-line-style-none invoice-padding-10">AMOUNT</li>
        <li className="invoice-line-style-none invoice-padding-2">TAX</li>
      </ul>
      {invoiceItems.map((item) => (
        <EachItem
          deleteItem = {deleteItem}
          itemDescription = {itemDescription}
          itemAdditionalDetails = {itemAdditionalDetails}
          itemRate = {itemRate}
          itemQuantity = {itemQuantity}
          // itemTotal = {itemTotal}
          itemTaxCheck = {itemTaxCheck}
          itemsCheckChange = {itemsCheckChange}
          key={item.id}
          itemDetails={item}
        />
      ))}
      <div className="invoice-dashed-line pb-3">
        <button onClick={addItem} className="invoice-add-item-button">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="currentColor"
            className="bi bi-plus invoice-add-item-icon"
            viewBox="0 0 16 16"
          >
            <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
          </svg>
        </button>
      </div>
      <div className="d-flex justify-content-end">
        <ul className="p-0 col-4 mt-3">
          <li className="invoice-line-style-none d-flex justify-content-between">
            <span>Subtotal</span>
            <span>
              ₹
              {invoiceItems
                .reduce((acc, cur) => {
                  acc += Number(cur.rate) * cur.quantity * 1.0;
                  return acc;
                }, 0.0)
                .toFixed(2)}
            </span>
          </li>
          <li className="invoice-line-style-none d-flex justify-content-between">
            <span>Tax (0%)</span>
            <span>₹0.00</span>
          </li>
          <li className="invoice-line-style-none d-flex justify-content-between">
            <span>Total</span>
            <span>
              ₹
              {invoiceItems
                .reduce((acc, cur) => {
                  acc += Number(cur.rate) * cur.quantity * 1.0;
                  return acc;
                }, 0.0)
                .toFixed(2)}
            </span>
          </li>
          <li className="invoice-line-style-none d-flex justify-content-between">
            <span>Balance Due</span>
            <span>
              ₹
              {invoiceItems
                .reduce((acc, cur) => {
                  acc += Number(cur.rate) * cur.quantity * 1.0;
                  return acc;
                }, 0.0)
                .toFixed(2)}
            </span>
          </li>
        </ul>
      </div>
      <div className="d-flex flex-column">
        <label htmlFor="">Notes</label>
        <textarea
          id=""
          className="form-control invoice-notes-input"
          placeholder="Notes - any relevant information not covered, additional terms and conditions"
          onChange={invoiceNotesChange}
          value={invoiceNotes}
        ></textarea>
      </div>
      <div className="d-flex justify-content-end mt-5">
        <Button
          type="submit"
          className="btn btn-outline-secondary invoice-generate-invoice-button"
          onClick={generateInvoiceButton}
          disabled={finalInvoiceButton}
        >
          Generate Invoice
        </Button>
        <Modal
          title={InvoiceTitle}
          visible={displayFinalInvoice}
          onOk={confirmFinalInvoice}
          onCancel={closeFinalInvoice}
        >
          <ul className="invoice-final-invoice-main-container">
            <li className="invoice-line-style-none">
              <h5>From</h5>
              <hr className="mt-0" />
            </li>
            <li className="invoice-line-style-none">
              <h6>Name: {from.name}</h6>
            </li>
            <li className="invoice-line-style-none">
              <h6>Email: {from.email}</h6>
            </li>
            <li className="invoice-line-style-none">
              <h6>Phone: {from.phone}</h6>
            </li>
            <li className="invoice-line-style-none">
              <h6>
                {from.address.street}, {from.address.cityState},
                {from.address.pinCode}
              </h6>
            </li>
          </ul>
          <ul className="invoice-final-invoice-main-container">
            <li className="invoice-line-style-none">
              <h5>Bill To</h5>
              <hr className="mt-0" />
            </li>
            <li className="invoice-line-style-none">
              <h6>Name: {billTo.name}</h6>
            </li>
            <li className="invoice-line-style-none">
              <h6>Email: {billTo.email}</h6>
            </li>
            <li className="invoice-line-style-none">
              <h6>Phone: {billTo.phone}</h6>
            </li>
            <li className="invoice-line-style-none">
              <h6>
                {billTo.address.street}, {billTo.address.cityState},
                {billTo.address.pinCode}
              </h6>
            </li>
          </ul>
          <ul className="invoice-final-invoice-main-container">
            <li className="invoice-line-style-none invoice-width-40 d-flex justify-content-between">
              <h6>Number: </h6>
              <span>{dataTime.number}</span>
            </li>
            <li className="invoice-line-style-none invoice-width-40 d-flex justify-content-between">
              <h6>Date: </h6>
              <span>{date.format("DD/MM/YYYY")}</span>
            </li>
            <li className="invoice-line-style-none invoice-width-40 d-flex justify-content-between">
              <h6>Duedate: </h6>
              <span>
                {date.format("DD/MM/YYYY") === dueDate.format("DD/MM/YYYY")
                  ? "N/A"
                  : dueDate.format("DD/MM/YYYY")}
              </span>
            </li>
            <li className="invoice-line-style-none">
              <Table
                pagination={false}
                columns={columns}
                dataSource={invoiceItems}
                size="small"
              />
            </li>
            <li className="invoice-line-style-none mt-2 invoice-width-40 d-flex justify-content-between">
              <h6>Total: </h6>
              <span>
                ₹
                {invoiceItems
                  .reduce((acc, cur) => {
                    acc += Number(cur.rate) * cur.quantity * 1.0;
                    return acc;
                  }, 0.0)
                  .toFixed(2)}
              </span>
            </li>
          </ul>
          <ul className="invoice-final-invoice-main-container">
            <li className="invoice-line-style-none">
              <h5>Notes</h5>
              <hr className="mt-0" />
              <p>{invoiceNotes}</p>
            </li>
          </ul>
        </Modal>
      </div>
    </div>
  );
}

export default InvoiceItemsSection;
