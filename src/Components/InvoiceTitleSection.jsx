import React from "react";
import { Upload } from "antd";


function InvoiceTitleSection(props) {

  const {
    invoicetitleChange,
    invoicetitleBlur,
    InvoiceTitleCheck,
    InvoiceTitleErrorText,
    fileListData,
    imageChange,
    imagePreview,
    InvoiceTitle
  } = props;

  return (
    <div className="d-flex justify-content-between">
      <div>
        <input
          type="text"
          className="form-control invoice-title-input"
          placeholder="Invoice"
          onChange={invoicetitleChange}
          value={InvoiceTitle}
          onBlur={invoicetitleBlur}
        />
        <p
          className={
            InvoiceTitleCheck ? "d-none" : "m-0 text-danger error-text"
          }
        >
          {InvoiceTitleErrorText}
        </p>
      </div>
      <Upload
        className="invoice-logo-image" 
        action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
        listType="picture-card"
        fileList={fileListData}
        onChange={imageChange}
        onPreview={imagePreview}
      >
        {fileListData.length < 1 && "+ Upload"}
      </Upload>
    </div>
  );
}

export default InvoiceTitleSection;
