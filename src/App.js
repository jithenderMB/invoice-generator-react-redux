import "./App.css";
import "./Styles/Invoice.css";


import InvoicePage from "./Components/InvoicePage";

function App() {
  return (
    <div className="main">
      <InvoicePage />
    </div>
  );
}

export default App;
