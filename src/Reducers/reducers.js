import { combineReducers } from "redux";
import { v4 as uuidv4 } from "uuid";
import {
  DISPLAY_ITEMS,
  ADD_ITEM,
  DELETE_ITEM,
  UPDATE_ITEM_DESCRIPTION,
  UPDATE_ADDTIONAL_DETAILS,
  ITEM_RATE,
  ITEM_QUANTITY,
  ITEM_TOTAL,
  UPDATE_TAX_CHECK,
} from "../Actions/namedActions";

const initialItems = () => {
  return {
    id: uuidv4(),
    itemDescription: "",
    addtionalDetails: "",
    rate: "",
    quantity: 1,
    amount: 0,
    taxCheck: false,
  };
};
const itemsReducer = (items = [], action) => {
  switch (action.type) {
    case DISPLAY_ITEMS:
      return [initialItems()];
    case ADD_ITEM:
      return [...items, initialItems()];
    case DELETE_ITEM:
      return items.filter((item) => item.id !== action.payload);
    case UPDATE_ITEM_DESCRIPTION:
      return items.map((item) =>
        item.id === action.id
          ? { ...item, itemDescription: action.payload }
          : item
      );
    case UPDATE_ADDTIONAL_DETAILS:
      return items.map((item) =>
        item.id === action.id
          ? { ...item, addtionalDetails: action.payload }
          : item
      );
    case ITEM_RATE:
      return items.map((item) =>
        item.id === action.id ? { ...item, rate: action.payload } : item
      );
    case ITEM_QUANTITY:
      return items.map((item) =>
        item.id === action.id ? { ...item, quantity: action.payload } : item
      );
    case ITEM_TOTAL:
      return items.map((item) =>
        item.id === action.id
          ? { ...item, amount: (item.rate * item.quantity * 1.0).toFixed(2) }
          : item
      );

    case UPDATE_TAX_CHECK:
      return items.map((item) =>
        item.id === action.id ? { ...item, taxCheck: action.payload } : item
      );
    default:
      return items;
  }
};

export default combineReducers({
  itemsReducer,
});
